﻿/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

namespace HowToDriver
{
  class Program
  {
    static void Main(string[] args)
    {
      HowTo_UseEngine.Example.Run();
      //HowTo_CreateState.Example.Run();
      //HowTo_PatientStates.Example.Run();

      //HowTo_ARDS.Example.Run();
      //HowTo_COPD.Example.Run();
      //HowTo_AnesthesiaMachine.Example.Run();
      //HowTo_Dehydration.Example.Run();
      //HowTo_Environment.Example.Run();
      //HowTo_Hemorrhage.Example.Run();
      //HowTo_ECMO.Example.Run();
      //HowTo_Hemothorax.Example.Run();
      //HowTo_MechanicalVentilator.Example.Run();
      //HowTo_Pneumonia.Example.Run();
      //HowTo_RespiratoryMechanics.Example.Run();
      //HowTo_SystemModifiers.Example.Run();
      //HowTo_DeathState.Example.Run();
      //HowTo_RunScenarios.Example.Run();
      //HowTo_VentilationMechanics.Example.Run();
      //HowTo_UseStates.Example.Run();
    }
  }
}
