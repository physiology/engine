\anchor MechanicalVentilatorARDS
@insert ./validation/scenarios/MechanicalVentilator/ARDS-Introduction.md

### Segment Validation

#### Segment 1

@insert ./validation/tables/MechanicalVentilator/ARDS/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/ARDS-vitals_monitor_1.jpg"><img src="./plots/MechanicalVentilator/ARDS-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS-ventilator_monitor_1.jpg"><img src="./plots/MechanicalVentilator/ARDS-ventilator_monitor_1.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS-ventilator_loops_1.jpg"><img src="./plots/MechanicalVentilator/ARDS-ventilator_loops_1.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {ARDSSegment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/ARDS/Segment1SegmentTable.md

#### Segment 2

@insert ./validation/tables/MechanicalVentilator/ARDS/Segment2ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/ARDS-vitals_monitor_2.jpg"><img src="./plots/MechanicalVentilator/ARDS-vitals_monitor_2.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS-ventilator_monitor_2.jpg"><img src="./plots/MechanicalVentilator/ARDS-ventilator_monitor_2.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS-ventilator_loops_2.jpg"><img src="./plots/MechanicalVentilator/ARDS-ventilator_loops_2.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {ARDSSegment2Monitors}. Vitals and ventilator monitors for Segment 2.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/ARDS/Segment2SegmentTable.md

#### Segment 3

@insert ./validation/tables/MechanicalVentilator/ARDS/Segment3ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/ARDS-vitals_monitor_3.jpg"><img src="./plots/MechanicalVentilator/ARDS-vitals_monitor_3.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS-ventilator_monitor_3.jpg"><img src="./plots/MechanicalVentilator/ARDS-ventilator_monitor_3.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS-ventilator_loops_3.jpg"><img src="./plots/MechanicalVentilator/ARDS-ventilator_loops_3.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {ARDSSegment3Monitors}. Vitals and ventilator monitors for Segment 3.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/ARDS/Segment3SegmentTable.md

### Scenario Output Plots

@htmlonly
<center>
<table border="0">
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS_TotalLungVolume.jpg"><img src="./plots/MechanicalVentilator/ARDS_TotalLungVolume.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS_RespirationRate.jpg"><img src="./plots/MechanicalVentilator/ARDS_RespirationRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS_TidalVolume.jpg"><img src="./plots/MechanicalVentilator/ARDS_TidalVolume.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS_HeartRate.jpg"><img src="./plots/MechanicalVentilator/ARDS_HeartRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS_MeanArterialPressure.jpg"><img src="./plots/MechanicalVentilator/ARDS_MeanArterialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS_ArterialPressure.jpg"><img src="./plots/MechanicalVentilator/ARDS_ArterialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS_Aorta-Oxygen-PartialPressure.jpg"><img src="./plots/MechanicalVentilator/ARDS_Aorta-Oxygen-PartialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS_Aorta-CarbonDioxide-PartialPressure.jpg"><img src="./plots/MechanicalVentilator/ARDS_Aorta-CarbonDioxide-PartialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS_ShuntFraction.jpg"><img src="./plots/MechanicalVentilator/ARDS_ShuntFraction.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS_AlveolarDeadSpace.jpg"><img src="./plots/MechanicalVentilator/ARDS_AlveolarDeadSpace.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS_PhysiologicDeadSpaceTidalVolumeRatio.jpg"><img src="./plots/MechanicalVentilator/ARDS_PhysiologicDeadSpaceTidalVolumeRatio.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS_VentilationPerfusionRatio.jpg"><img src="./plots/MechanicalVentilator/ARDS_VentilationPerfusionRatio.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS_TransthoracicPressure.jpg"><img src="./plots/MechanicalVentilator/ARDS_TransthoracicPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS_IntrinsicPositiveEndExpiratoryPressure.jpg"><img src="./plots/MechanicalVentilator/ARDS_IntrinsicPositiveEndExpiratoryPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/ARDS_Patient-FunctionalResidualCapacity.jpg"><img src="./plots/MechanicalVentilator/ARDS_Patient-FunctionalResidualCapacity.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/ARDS_Patient-AlveoliSurfaceArea.jpg"><img src="./plots/MechanicalVentilator/ARDS_Patient-AlveoliSurfaceArea.jpg" width="550"></a></td>
</tr>
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/ARDS_Legend.jpg"><img src="./plots/MechanicalVentilator/ARDS_Legend.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {ARDSOutputs}. Select outputs from the scenario.</i>
</center><br>