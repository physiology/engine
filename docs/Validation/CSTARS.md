## En Route Care Validation

### Introduction

Additional validation has been conducted for the management of ventilated patients during transport, encompassing a variety of conditions and injuries.

### Data Sources

Empirical data and published sources were utilized where available. In cases lacking direct data, expected patient outcomes were verified by the clinical expertise of subject matter experts @cite BransonSME, @cite ChatburnSME.

### Results

@secreflist
  @refitem CSTARSScenario1 "Scenario 1: Mild ARDS with Progressive Fall in Oxygenation"
  @refitem CSTARSScenario2 "Scenario 2: Moderate COPD with Subsequent Bronchospasm"
  @refitem CSTARSScenario3 "Scenario 3: Mild TBI with worsening ARDS"
  @refitem CSTARSScenario4 "Scenario 4: Moderate ARDS with Tension Pneumothorax"
  @refitem CSTARSScenario5 "Scenario 5: Moderate ARDS with Right Mainstem Intubation"
  @refitem CSTARSScenario6 "Scenario 6: Hypoxemic Respiratory Failure - Pneumonia"
  @refitem CSTARSScenario7 "Scenario 7: Hypoxemic Respiratory Failure Plugged ETT"
@endsecreflist

