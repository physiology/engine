\anchor MechanicalVentilatorRecruitment
@insert ./validation/scenarios/MechanicalVentilator/Recruitment-Introduction.md

### Segment Validation

#### Segment 1

@insert ./validation/tables/MechanicalVentilator/Recruitment/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/Recruitment-vitals_monitor_1.jpg"><img src="./plots/MechanicalVentilator/Recruitment-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment-ventilator_monitor_1.jpg"><img src="./plots/MechanicalVentilator/Recruitment-ventilator_monitor_1.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment-ventilator_loops_1.jpg"><img src="./plots/MechanicalVentilator/Recruitment-ventilator_loops_1.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {RecruitmentSegment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/Recruitment/Segment1SegmentTable.md

#### Segment 2

@insert ./validation/tables/MechanicalVentilator/Recruitment/Segment2ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/Recruitment-vitals_monitor_2.jpg"><img src="./plots/MechanicalVentilator/Recruitment-vitals_monitor_2.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment-ventilator_monitor_2.jpg"><img src="./plots/MechanicalVentilator/Recruitment-ventilator_monitor_2.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment-ventilator_loops_2.jpg"><img src="./plots/MechanicalVentilator/Recruitment-ventilator_loops_2.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {RecruitmentSegment2Monitors}. Vitals and ventilator monitors for Segment 2.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/Recruitment/Segment2SegmentTable.md

### Scenario Output Plots

@htmlonly
<center>
<table border="0">
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment_TotalLungVolume.jpg"><img src="./plots/MechanicalVentilator/Recruitment_TotalLungVolume.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment_RespirationRate.jpg"><img src="./plots/MechanicalVentilator/Recruitment_RespirationRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment_TidalVolume.jpg"><img src="./plots/MechanicalVentilator/Recruitment_TidalVolume.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment_HeartRate.jpg"><img src="./plots/MechanicalVentilator/Recruitment_HeartRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment_MeanArterialPressure.jpg"><img src="./plots/MechanicalVentilator/Recruitment_MeanArterialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment_ArterialPressure.jpg"><img src="./plots/MechanicalVentilator/Recruitment_ArterialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment_Aorta-Oxygen-PartialPressure.jpg"><img src="./plots/MechanicalVentilator/Recruitment_Aorta-Oxygen-PartialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment_Aorta-CarbonDioxide-PartialPressure.jpg"><img src="./plots/MechanicalVentilator/Recruitment_Aorta-CarbonDioxide-PartialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment_ShuntFraction.jpg"><img src="./plots/MechanicalVentilator/Recruitment_ShuntFraction.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment_AlveolarDeadSpace.jpg"><img src="./plots/MechanicalVentilator/Recruitment_AlveolarDeadSpace.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment_PhysiologicDeadSpaceTidalVolumeRatio.jpg"><img src="./plots/MechanicalVentilator/Recruitment_PhysiologicDeadSpaceTidalVolumeRatio.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment_VentilationPerfusionRatio.jpg"><img src="./plots/MechanicalVentilator/Recruitment_VentilationPerfusionRatio.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment_TransthoracicPressure.jpg"><img src="./plots/MechanicalVentilator/Recruitment_TransthoracicPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment_IntrinsicPositiveEndExpiratoryPressure.jpg"><img src="./plots/MechanicalVentilator/Recruitment_IntrinsicPositiveEndExpiratoryPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Recruitment_Patient-FunctionalResidualCapacity.jpg"><img src="./plots/MechanicalVentilator/Recruitment_Patient-FunctionalResidualCapacity.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Recruitment_Patient-AlveoliSurfaceArea.jpg"><img src="./plots/MechanicalVentilator/Recruitment_Patient-AlveoliSurfaceArea.jpg" width="550"></a></td>
</tr>
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/Recruitment_Legend.jpg"><img src="./plots/MechanicalVentilator/Recruitment_Legend.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {RecruitmentOutputs}. Select outputs from the scenario.</i>
</center><br>