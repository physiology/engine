# Dehydration Validation

The following list contains the scenarios validated for the dehydration model.

@secreflist
  @refitem HealthyHydration "Healthy Scenario"
  @refitem MildDehydration "Mild Dehydration Scenario"
  @refitem ModerateDehydration "Moderate Dehydration Scenario"
  @refitem SevereDehydration "Severe Dehydration Scenario"
@endsecreflist
