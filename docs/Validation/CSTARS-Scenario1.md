CSTARS Scenario 1 {#CSTARSScenario1}
====================================

@insert ./validation/scenarios/CSTARS/Scenario1-Introduction.md

@htmlonly
<a href="./Images/CSTARS/MildARDSXRay.png"><img src="./Images/CSTARS/MildARDSXRay.png" width="400"></a>
<center>
<i>@figuredef {Scenario1XRay}. Chest radiograph demonstrates bi-basilar early consolidation, presence of the chest tube and pulmonary contusion.</i>
</center><br>
@endhtmlonly

### Segment Validation

#### Segment 1

@insert ./validation/tables/CSTARS/Scenario1/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario1-vitals_monitor_1.jpg"><img src="./plots/CSTARS/Scenario1-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1-ventilator_monitor_1.jpg"><img src="./plots/CSTARS/Scenario1-ventilator_monitor_1.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario1-ventilator_loops_1.jpg"><img src="./plots/CSTARS/Scenario1-ventilator_loops_1.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario1Segment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario1/Segment1SegmentTable.md

#### Segment 2

@insert ./validation/tables/CSTARS/Scenario1/Segment2ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario1-vitals_monitor_2.jpg"><img src="./plots/CSTARS/Scenario1-vitals_monitor_2.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1-ventilator_monitor_2.jpg"><img src="./plots/CSTARS/Scenario1-ventilator_monitor_2.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario1-ventilator_loops_2.jpg"><img src="./plots/CSTARS/Scenario1-ventilator_loops_2.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario1Segment2Monitors}. Vitals and ventilator monitors for Segment 2.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario1/Segment2SegmentTable.md

#### Segment 3

@insert ./validation/tables/CSTARS/Scenario1/Segment3ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario1-vitals_monitor_3.jpg"><img src="./plots/CSTARS/Scenario1-vitals_monitor_3.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1-ventilator_monitor_3.jpg"><img src="./plots/CSTARS/Scenario1-ventilator_monitor_3.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario1-ventilator_loops_3.jpg"><img src="./plots/CSTARS/Scenario1-ventilator_loops_3.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario1Segment3Monitors}. Vitals and ventilator monitors for Segment 3.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario1/Segment3SegmentTable.md

#### Segment 4

@insert ./validation/tables/CSTARS/Scenario1/Segment4ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario1-vitals_monitor_4.jpg"><img src="./plots/CSTARS/Scenario1-vitals_monitor_4.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1-ventilator_monitor_4.jpg"><img src="./plots/CSTARS/Scenario1-ventilator_monitor_4.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario1-ventilator_loops_4.jpg"><img src="./plots/CSTARS/Scenario1-ventilator_loops_4.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario1Segment4Monitors}. Vitals and ventilator monitors for Segment 4.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario1/Segment4SegmentTable.md


### Scenario Output Plots

@htmlonly
<center>
<table border="0">
<tr>
    <td><a href="./plots/CSTARS/Scenario1_TotalLungVolume.jpg"><img src="./plots/CSTARS/Scenario1_TotalLungVolume.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario1_RespirationRate.jpg"><img src="./plots/CSTARS/Scenario1_RespirationRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1_TidalVolume.jpg"><img src="./plots/CSTARS/Scenario1_TidalVolume.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario1_HeartRate.jpg"><img src="./plots/CSTARS/Scenario1_HeartRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1_MeanArterialPressure.jpg"><img src="./plots/CSTARS/Scenario1_MeanArterialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario1_ArterialPressure.jpg"><img src="./plots/CSTARS/Scenario1_ArterialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1_Aorta-Oxygen-PartialPressure.jpg"><img src="./plots/CSTARS/Scenario1_Aorta-Oxygen-PartialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario1_Aorta-CarbonDioxide-PartialPressure.jpg"><img src="./plots/CSTARS/Scenario1_Aorta-CarbonDioxide-PartialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1_ShuntFraction.jpg"><img src="./plots/CSTARS/Scenario1_ShuntFraction.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario1_AlveolarDeadSpace.jpg"><img src="./plots/CSTARS/Scenario1_AlveolarDeadSpace.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1_PhysiologicDeadSpaceTidalVolumeRatio.jpg"><img src="./plots/CSTARS/Scenario1_PhysiologicDeadSpaceTidalVolumeRatio.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario1_VentilationPerfusionRatio.jpg"><img src="./plots/CSTARS/Scenario1_VentilationPerfusionRatio.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1_TransthoracicPressure.jpg"><img src="./plots/CSTARS/Scenario1_TransthoracicPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario1_IntrinsicPositiveEndExpiratoryPressure.jpg"><img src="./plots/CSTARS/Scenario1_IntrinsicPositiveEndExpiratoryPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario1_Patient-FunctionalResidualCapacity.jpg"><img src="./plots/CSTARS/Scenario1_Patient-FunctionalResidualCapacity.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario1_Patient-AlveoliSurfaceArea.jpg"><img src="./plots/CSTARS/Scenario1_Patient-AlveoliSurfaceArea.jpg" width="550"></a></td>
</tr>
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario1_Legend.jpg"><img src="./plots/CSTARS/Scenario1_Legend.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario1Outputs}. Select outputs from the scenario.</i>
</center><br>