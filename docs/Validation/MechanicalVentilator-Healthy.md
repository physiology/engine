\anchor MechanicalVentilatorHealthy
@insert ./validation/scenarios/MechanicalVentilator/Healthy-Introduction.md

### Segment Validation

#### Segment 1

@insert ./validation/tables/MechanicalVentilator/Healthy/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/Healthy-vitals_monitor_1.jpg"><img src="./plots/MechanicalVentilator/Healthy-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy-ventilator_monitor_1.jpg"><img src="./plots/MechanicalVentilator/Healthy-ventilator_monitor_1.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy-ventilator_loops_1.jpg"><img src="./plots/MechanicalVentilator/Healthy-ventilator_loops_1.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {HealthySegment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/Healthy/Segment1SegmentTable.md

### Scenario Output Plots

@htmlonly
<center>
<table border="0">
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy_TotalLungVolume.jpg"><img src="./plots/MechanicalVentilator/Healthy_TotalLungVolume.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy_RespirationRate.jpg"><img src="./plots/MechanicalVentilator/Healthy_RespirationRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy_TidalVolume.jpg"><img src="./plots/MechanicalVentilator/Healthy_TidalVolume.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy_HeartRate.jpg"><img src="./plots/MechanicalVentilator/Healthy_HeartRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy_MeanArterialPressure.jpg"><img src="./plots/MechanicalVentilator/Healthy_MeanArterialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy_ArterialPressure.jpg"><img src="./plots/MechanicalVentilator/Healthy_ArterialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy_Aorta-Oxygen-PartialPressure.jpg"><img src="./plots/MechanicalVentilator/Healthy_Aorta-Oxygen-PartialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy_Aorta-CarbonDioxide-PartialPressure.jpg"><img src="./plots/MechanicalVentilator/Healthy_Aorta-CarbonDioxide-PartialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy_ShuntFraction.jpg"><img src="./plots/MechanicalVentilator/Healthy_ShuntFraction.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy_AlveolarDeadSpace.jpg"><img src="./plots/MechanicalVentilator/Healthy_AlveolarDeadSpace.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy_PhysiologicDeadSpaceTidalVolumeRatio.jpg"><img src="./plots/MechanicalVentilator/Healthy_PhysiologicDeadSpaceTidalVolumeRatio.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy_VentilationPerfusionRatio.jpg"><img src="./plots/MechanicalVentilator/Healthy_VentilationPerfusionRatio.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy_TransthoracicPressure.jpg"><img src="./plots/MechanicalVentilator/Healthy_TransthoracicPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy_IntrinsicPositiveEndExpiratoryPressure.jpg"><img src="./plots/MechanicalVentilator/Healthy_IntrinsicPositiveEndExpiratoryPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/Healthy_Patient-FunctionalResidualCapacity.jpg"><img src="./plots/MechanicalVentilator/Healthy_Patient-FunctionalResidualCapacity.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/Healthy_Patient-AlveoliSurfaceArea.jpg"><img src="./plots/MechanicalVentilator/Healthy_Patient-AlveoliSurfaceArea.jpg" width="550"></a></td>
</tr>
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/Healthy_Legend.jpg"><img src="./plots/MechanicalVentilator/Healthy_Legend.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {HealthyOutputs}. Select outputs from the scenario.</i>
</center><br>