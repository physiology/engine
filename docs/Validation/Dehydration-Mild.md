\anchor MildDehydration
@insert ./validation/scenarios/Dehydration/Mild-Introduction.md

### Segment Validation

#### Segment 1

@insert ./validation/tables/Dehydration/Mild/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/Dehydration/Mild-vitals_monitor_1.jpg"><img src="./plots/Dehydration/Mild-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {MildDehydrationSegment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/Dehydration/Mild/Segment1SegmentTable.md
