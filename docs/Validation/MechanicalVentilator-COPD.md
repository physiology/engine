\anchor MechanicalVentilatorCOPD
@insert ./validation/scenarios/MechanicalVentilator/COPD-Introduction.md

### Segment Validation

#### Segment 1

@insert ./validation/tables/MechanicalVentilator/COPD/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/COPD-vitals_monitor_1.jpg"><img src="./plots/MechanicalVentilator/COPD-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD-ventilator_monitor_1.jpg"><img src="./plots/MechanicalVentilator/COPD-ventilator_monitor_1.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD-ventilator_loops_1.jpg"><img src="./plots/MechanicalVentilator/COPD-ventilator_loops_1.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {COPDSegment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/COPD/Segment1SegmentTable.md

#### Segment 2

@insert ./validation/tables/MechanicalVentilator/COPD/Segment2ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/COPD-vitals_monitor_2.jpg"><img src="./plots/MechanicalVentilator/COPD-vitals_monitor_2.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD-ventilator_monitor_2.jpg"><img src="./plots/MechanicalVentilator/COPD-ventilator_monitor_2.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD-ventilator_loops_2.jpg"><img src="./plots/MechanicalVentilator/COPD-ventilator_loops_2.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {COPDSegment2Monitors}. Vitals and ventilator monitors for Segment 2.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/COPD/Segment2SegmentTable.md

#### Segment 3

@insert ./validation/tables/MechanicalVentilator/COPD/Segment3ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/COPD-vitals_monitor_3.jpg"><img src="./plots/MechanicalVentilator/COPD-vitals_monitor_3.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD-ventilator_monitor_3.jpg"><img src="./plots/MechanicalVentilator/COPD-ventilator_monitor_3.jpg" width="825"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD-ventilator_loops_3.jpg"><img src="./plots/MechanicalVentilator/COPD-ventilator_loops_3.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {COPDSegment3Monitors}. Vitals and ventilator monitors for Segment 3.</i>
</center><br>

@insert ./validation/tables/MechanicalVentilator/COPD/Segment3SegmentTable.md

### Scenario Output Plots

@htmlonly
<center>
<table border="0">
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD_TotalLungVolume.jpg"><img src="./plots/MechanicalVentilator/COPD_TotalLungVolume.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD_RespirationRate.jpg"><img src="./plots/MechanicalVentilator/COPD_RespirationRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD_TidalVolume.jpg"><img src="./plots/MechanicalVentilator/COPD_TidalVolume.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD_HeartRate.jpg"><img src="./plots/MechanicalVentilator/COPD_HeartRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD_MeanArterialPressure.jpg"><img src="./plots/MechanicalVentilator/COPD_MeanArterialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD_ArterialPressure.jpg"><img src="./plots/MechanicalVentilator/COPD_ArterialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD_Aorta-Oxygen-PartialPressure.jpg"><img src="./plots/MechanicalVentilator/COPD_Aorta-Oxygen-PartialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD_Aorta-CarbonDioxide-PartialPressure.jpg"><img src="./plots/MechanicalVentilator/COPD_Aorta-CarbonDioxide-PartialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD_ShuntFraction.jpg"><img src="./plots/MechanicalVentilator/COPD_ShuntFraction.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD_AlveolarDeadSpace.jpg"><img src="./plots/MechanicalVentilator/COPD_AlveolarDeadSpace.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD_PhysiologicDeadSpaceTidalVolumeRatio.jpg"><img src="./plots/MechanicalVentilator/COPD_PhysiologicDeadSpaceTidalVolumeRatio.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD_VentilationPerfusionRatio.jpg"><img src="./plots/MechanicalVentilator/COPD_VentilationPerfusionRatio.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD_TransthoracicPressure.jpg"><img src="./plots/MechanicalVentilator/COPD_TransthoracicPressure.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD_IntrinsicPositiveEndExpiratoryPressure.jpg"><img src="./plots/MechanicalVentilator/COPD_IntrinsicPositiveEndExpiratoryPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/MechanicalVentilator/COPD_Patient-FunctionalResidualCapacity.jpg"><img src="./plots/MechanicalVentilator/COPD_Patient-FunctionalResidualCapacity.jpg" width="550"></a></td>
    <td><a href="./plots/MechanicalVentilator/COPD_Patient-AlveoliSurfaceArea.jpg"><img src="./plots/MechanicalVentilator/COPD_Patient-AlveoliSurfaceArea.jpg" width="550"></a></td>
</tr>
<tr>
    <td colspan="2"><a href="./plots/MechanicalVentilator/COPD_Legend.jpg"><img src="./plots/MechanicalVentilator/COPD_Legend.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {COPDOutputs}. Select outputs from the scenario.</i>
</center><br>