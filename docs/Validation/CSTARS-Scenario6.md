CSTARS Scenario 6 {#CSTARSScenario6}
====================================

@insert ./validation/scenarios/CSTARS/Scenario6-Introduction.md

@htmlonly
<a href="./Images/CSTARS/Placeholder.png"><img src="./Images/CSTARS/Placeholder.png" width="400"></a>
<center>
<i>@figuredef {Scenario2XRay}. Chest radiograph demonstrates bilateral infiltrates with right middle and lower lobe consolidation, but no signs of barotrauma.</i>
</center><br>
@endhtmlonly

### Segment Validation

#### Segment 1

@insert ./validation/tables/CSTARS/Scenario6/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario6-vitals_monitor_1.jpg"><img src="./plots/CSTARS/Scenario6-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6-ventilator_monitor_1.jpg"><img src="./plots/CSTARS/Scenario6-ventilator_monitor_1.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario6-ventilator_loops_1.jpg"><img src="./plots/CSTARS/Scenario6-ventilator_loops_1.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario6Segment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario6/Segment1SegmentTable.md

#### Segment 2

@insert ./validation/tables/CSTARS/Scenario6/Segment2ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario6-vitals_monitor_2.jpg"><img src="./plots/CSTARS/Scenario6-vitals_monitor_2.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6-ventilator_monitor_2.jpg"><img src="./plots/CSTARS/Scenario6-ventilator_monitor_2.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario6-ventilator_loops_2.jpg"><img src="./plots/CSTARS/Scenario6-ventilator_loops_2.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario6Segment2Monitors}. Vitals and ventilator monitors for Segment 2.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario6/Segment2SegmentTable.md

#### Segment 3

@insert ./validation/tables/CSTARS/Scenario6/Segment3ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario6-vitals_monitor_3.jpg"><img src="./plots/CSTARS/Scenario6-vitals_monitor_3.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6-ventilator_monitor_3.jpg"><img src="./plots/CSTARS/Scenario6-ventilator_monitor_3.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario6-ventilator_loops_3.jpg"><img src="./plots/CSTARS/Scenario6-ventilator_loops_3.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario6Segment3Monitors}. Vitals and ventilator monitors for Segment 3.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario6/Segment3SegmentTable.md

### Scenario Output Plots

@htmlonly
<center>
<table border="0">
<tr>
    <td><a href="./plots/CSTARS/Scenario6_TotalLungVolume.jpg"><img src="./plots/CSTARS/Scenario6_TotalLungVolume.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario6_RespirationRate.jpg"><img src="./plots/CSTARS/Scenario6_RespirationRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6_TidalVolume.jpg"><img src="./plots/CSTARS/Scenario6_TidalVolume.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario6_HeartRate.jpg"><img src="./plots/CSTARS/Scenario6_HeartRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6_MeanArterialPressure.jpg"><img src="./plots/CSTARS/Scenario6_MeanArterialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario6_ArterialPressure.jpg"><img src="./plots/CSTARS/Scenario6_ArterialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6_Aorta-Oxygen-PartialPressure.jpg"><img src="./plots/CSTARS/Scenario6_Aorta-Oxygen-PartialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario6_Aorta-CarbonDioxide-PartialPressure.jpg"><img src="./plots/CSTARS/Scenario6_Aorta-CarbonDioxide-PartialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6_ShuntFraction.jpg"><img src="./plots/CSTARS/Scenario6_ShuntFraction.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario6_AlveolarDeadSpace.jpg"><img src="./plots/CSTARS/Scenario6_AlveolarDeadSpace.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6_PhysiologicDeadSpaceTidalVolumeRatio.jpg"><img src="./plots/CSTARS/Scenario6_PhysiologicDeadSpaceTidalVolumeRatio.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario6_VentilationPerfusionRatio.jpg"><img src="./plots/CSTARS/Scenario6_VentilationPerfusionRatio.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6_TransthoracicPressure.jpg"><img src="./plots/CSTARS/Scenario6_TransthoracicPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario6_IntrinsicPositiveEndExpiratoryPressure.jpg"><img src="./plots/CSTARS/Scenario6_IntrinsicPositiveEndExpiratoryPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario6_Patient-FunctionalResidualCapacity.jpg"><img src="./plots/CSTARS/Scenario6_Patient-FunctionalResidualCapacity.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario6_Patient-AlveoliSurfaceArea.jpg"><img src="./plots/CSTARS/Scenario6_Patient-AlveoliSurfaceArea.jpg" width="550"></a></td>
</tr>
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario6_Legend.jpg"><img src="./plots/CSTARS/Scenario6_Legend.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario6Outputs}. Select outputs from the scenario.</i>
</center><br>
