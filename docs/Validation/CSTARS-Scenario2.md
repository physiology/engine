CSTARS Scenario 2 {#CSTARSScenario2}
====================================

@insert ./validation/scenarios/CSTARS/Scenario2-Introduction.md

@htmlonly
<a href="./Images/CSTARS/Placeholder.png"><img src="./Images/CSTARS/Placeholder.png" width="400"></a>
<center>
<i>@figuredef {Scenario2XRay}. Chest radiograph demonstrates left-lower lobe consolidation, haziness in the right lower lobe, flattened diaphragms and hyperinflation in the upper lung fields.</i>
</center><br>
@endhtmlonly

### Segment Validation

#### Segment 1

@insert ./validation/tables/CSTARS/Scenario2/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario2-vitals_monitor_1.jpg"><img src="./plots/CSTARS/Scenario2-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2-ventilator_monitor_1.jpg"><img src="./plots/CSTARS/Scenario2-ventilator_monitor_1.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario2-ventilator_loops_1.jpg"><img src="./plots/CSTARS/Scenario2-ventilator_loops_1.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario2Segment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario2/Segment1SegmentTable.md

#### Segment 2

@insert ./validation/tables/CSTARS/Scenario2/Segment2ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario2-vitals_monitor_2.jpg"><img src="./plots/CSTARS/Scenario2-vitals_monitor_2.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2-ventilator_monitor_2.jpg"><img src="./plots/CSTARS/Scenario2-ventilator_monitor_2.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario2-ventilator_loops_2.jpg"><img src="./plots/CSTARS/Scenario2-ventilator_loops_2.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario2Segment2Monitors}. Vitals and ventilator monitors for Segment 2.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario2/Segment2SegmentTable.md

#### Segment 3

@insert ./validation/tables/CSTARS/Scenario2/Segment3ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario2-vitals_monitor_3.jpg"><img src="./plots/CSTARS/Scenario2-vitals_monitor_3.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2-ventilator_monitor_3.jpg"><img src="./plots/CSTARS/Scenario2-ventilator_monitor_3.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario2-ventilator_loops_3.jpg"><img src="./plots/CSTARS/Scenario2-ventilator_loops_3.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario2Segment3Monitors}. Vitals and ventilator monitors for Segment 3.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario2/Segment3SegmentTable.md

### Scenario Output Plots

@htmlonly
<center>
<table border="0">
<tr>
    <td><a href="./plots/CSTARS/Scenario2_TotalLungVolume.jpg"><img src="./plots/CSTARS/Scenario2_TotalLungVolume.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario2_RespirationRate.jpg"><img src="./plots/CSTARS/Scenario2_RespirationRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2_TidalVolume.jpg"><img src="./plots/CSTARS/Scenario2_TidalVolume.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario2_HeartRate.jpg"><img src="./plots/CSTARS/Scenario2_HeartRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2_MeanArterialPressure.jpg"><img src="./plots/CSTARS/Scenario2_MeanArterialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario2_ArterialPressure.jpg"><img src="./plots/CSTARS/Scenario2_ArterialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2_Aorta-Oxygen-PartialPressure.jpg"><img src="./plots/CSTARS/Scenario2_Aorta-Oxygen-PartialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario2_Aorta-CarbonDioxide-PartialPressure.jpg"><img src="./plots/CSTARS/Scenario2_Aorta-CarbonDioxide-PartialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2_ShuntFraction.jpg"><img src="./plots/CSTARS/Scenario2_ShuntFraction.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario2_AlveolarDeadSpace.jpg"><img src="./plots/CSTARS/Scenario2_AlveolarDeadSpace.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2_PhysiologicDeadSpaceTidalVolumeRatio.jpg"><img src="./plots/CSTARS/Scenario2_PhysiologicDeadSpaceTidalVolumeRatio.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario2_VentilationPerfusionRatio.jpg"><img src="./plots/CSTARS/Scenario2_VentilationPerfusionRatio.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2_TransthoracicPressure.jpg"><img src="./plots/CSTARS/Scenario2_TransthoracicPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario2_IntrinsicPositiveEndExpiratoryPressure.jpg"><img src="./plots/CSTARS/Scenario2_IntrinsicPositiveEndExpiratoryPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario2_Patient-FunctionalResidualCapacity.jpg"><img src="./plots/CSTARS/Scenario2_Patient-FunctionalResidualCapacity.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario2_Patient-AlveoliSurfaceArea.jpg"><img src="./plots/CSTARS/Scenario2_Patient-AlveoliSurfaceArea.jpg" width="550"></a></td>
</tr>
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario2_Legend.jpg"><img src="./plots/CSTARS/Scenario2_Legend.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario2Outputs}. Select outputs from the scenario.</i>
</center><br>
