CSTARS Scenario 3 {#CSTARSScenario3}
====================================

@insert ./validation/scenarios/CSTARS/Scenario3-Introduction.md

@htmlonly
<a href="./Images/CSTARS/Placeholder.png"><img src="./Images/CSTARS/Placeholder.png" width="400"></a>
<center>
<i>@figuredef {Scenario2XRay}. Chest radiograph demonstrates bilateral patchy opacities and ground glass appearance.</i>
</center><br>
@endhtmlonly

### Segment Validation

#### Segment 1

@insert ./validation/tables/CSTARS/Scenario3/Segment1ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario3-vitals_monitor_1.jpg"><img src="./plots/CSTARS/Scenario3-vitals_monitor_1.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3-ventilator_monitor_1.jpg"><img src="./plots/CSTARS/Scenario3-ventilator_monitor_1.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario3-ventilator_loops_1.jpg"><img src="./plots/CSTARS/Scenario3-ventilator_loops_1.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario3Segment1Monitors}. Vitals and ventilator monitors for Segment 1.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario3/Segment1SegmentTable.md

#### Segment 2

@insert ./validation/tables/CSTARS/Scenario3/Segment2ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario3-vitals_monitor_2.jpg"><img src="./plots/CSTARS/Scenario3-vitals_monitor_2.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3-ventilator_monitor_2.jpg"><img src="./plots/CSTARS/Scenario3-ventilator_monitor_2.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario3-ventilator_loops_2.jpg"><img src="./plots/CSTARS/Scenario3-ventilator_loops_2.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario3Segment2Monitors}. Vitals and ventilator monitors for Segment 2.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario3/Segment2SegmentTable.md

#### Segment 3

@insert ./validation/tables/CSTARS/Scenario3/Segment3ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario3-vitals_monitor_3.jpg"><img src="./plots/CSTARS/Scenario3-vitals_monitor_3.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3-ventilator_monitor_3.jpg"><img src="./plots/CSTARS/Scenario3-ventilator_monitor_3.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario3-ventilator_loops_3.jpg"><img src="./plots/CSTARS/Scenario3-ventilator_loops_3.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario3Segment3Monitors}. Vitals and ventilator monitors for Segment 3.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario3/Segment3SegmentTable.md

#### Segment 4

@insert ./validation/tables/CSTARS/Scenario3/Segment4ValidationTable.md

@htmlonly
<center>
<table border="0">
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario3-vitals_monitor_4.jpg"><img src="./plots/CSTARS/Scenario3-vitals_monitor_4.jpg" width="1100"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3-ventilator_monitor_4.jpg"><img src="./plots/CSTARS/Scenario3-ventilator_monitor_4.jpg" width="825"></a></td>
    <td><a href="./plots/CSTARS/Scenario3-ventilator_loops_4.jpg"><img src="./plots/CSTARS/Scenario3-ventilator_loops_4.jpg" width="275"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario3Segment4Monitors}. Vitals and ventilator monitors for Segment 4.</i>
</center><br>

@insert ./validation/tables/CSTARS/Scenario3/Segment4SegmentTable.md

### Scenario Output Plots

@htmlonly
<center>
<table border="0">
<tr>
    <td><a href="./plots/CSTARS/Scenario3_TotalLungVolume.jpg"><img src="./plots/CSTARS/Scenario3_TotalLungVolume.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario3_RespirationRate.jpg"><img src="./plots/CSTARS/Scenario3_RespirationRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3_TidalVolume.jpg"><img src="./plots/CSTARS/Scenario3_TidalVolume.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario3_HeartRate.jpg"><img src="./plots/CSTARS/Scenario3_HeartRate.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3_MeanArterialPressure.jpg"><img src="./plots/CSTARS/Scenario3_MeanArterialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario3_ArterialPressure.jpg"><img src="./plots/CSTARS/Scenario3_ArterialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3_Aorta-Oxygen-PartialPressure.jpg"><img src="./plots/CSTARS/Scenario3_Aorta-Oxygen-PartialPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario3_Aorta-CarbonDioxide-PartialPressure.jpg"><img src="./plots/CSTARS/Scenario3_Aorta-CarbonDioxide-PartialPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3_ShuntFraction.jpg"><img src="./plots/CSTARS/Scenario3_ShuntFraction.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario3_AlveolarDeadSpace.jpg"><img src="./plots/CSTARS/Scenario3_AlveolarDeadSpace.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3_PhysiologicDeadSpaceTidalVolumeRatio.jpg"><img src="./plots/CSTARS/Scenario3_PhysiologicDeadSpaceTidalVolumeRatio.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario3_VentilationPerfusionRatio.jpg"><img src="./plots/CSTARS/Scenario3_VentilationPerfusionRatio.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3_TransthoracicPressure.jpg"><img src="./plots/CSTARS/Scenario3_TransthoracicPressure.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario3_IntrinsicPositiveEndExpiratoryPressure.jpg"><img src="./plots/CSTARS/Scenario3_IntrinsicPositiveEndExpiratoryPressure.jpg" width="550"></a></td>
</tr>
<tr>
    <td><a href="./plots/CSTARS/Scenario3_Patient-FunctionalResidualCapacity.jpg"><img src="./plots/CSTARS/Scenario3_Patient-FunctionalResidualCapacity.jpg" width="550"></a></td>
    <td><a href="./plots/CSTARS/Scenario3_Patient-AlveoliSurfaceArea.jpg"><img src="./plots/CSTARS/Scenario3_Patient-AlveoliSurfaceArea.jpg" width="550"></a></td>
</tr>
<tr>
    <td colspan="2"><a href="./plots/CSTARS/Scenario3_Legend.jpg"><img src="./plots/CSTARS/Scenario3_Legend.jpg" width="1100"></a></td>
</tr>
</table>
<br>
</center>
@endhtmlonly
<center>
<i>@figuredef {Scenario3Outputs}. Select outputs from the scenario.</i>
</center><br>
